#!/bin/bash

mkdir "backend"

mkdir "frontend"

git clone https://gitlab.com/micro_project/backend.git backend

cd backend && composer install

sleep 3

cd ../

git clone https://gitlab.com/micro_project/frontend.git frontend

cd frontend && npm ci

sleep 3

ng build

exec "$@"