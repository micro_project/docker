#!/bin/bash

until php bin/console doctrine:query:sql "SELECT 1"; do
  echo "Waiting for MySQL to be ready..."
  sleep 1
done

php bin/console doctrine:migrations:migrate --no-interaction

sleep 10

if [ "$1" = "doctrine:fixtures:load" ]; then
    # Set DATABASE_URL for 'doctrine:fixtures:load' command to 0.0.0.0
    export DATABASE_URL="mysql://$MYSQL_USER:$MYSQL_PASSWORD@0.0.0.0:$MYSQL_PORT/$MYSQL_DATABASE?serverVersion=16&charset=utf8"

    php bin/console doctrine:fixtures:load --no-interaction
fi

php bin/console doctrine:fixtures:load --no-interaction

sleep 3

composer install

exec "$@"
