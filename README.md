# Micro project

Crud application in architecture Rest api with technology backend and frontend

## Structure database in UML
file UML_DATABASE.png

## Stack technology

- Docker-compose
- PHP 8.3
- Javascript
- Angular 17
- Symfony 7.0

## Proccess

### hosts

First step prepare hosts `` /etc/hosts `` copy this domains

- ``127.0.0.1       backendmicroapp.refs``<br>
  ``127.0.0.1       frontendmicroapp.refs``


### directory structure 

project
- docker
- - nginx
- - php
- - .env
- - docker-compose.yaml
- - dockerfile
- - dockerfilePHP
- - docker-entrypoint.sh
- - UML_DATABASE.png
- - frontend `` have whole angular directories and files``
- - backend `` the same like frontend but symfony ``

## Instalation

- `` git clone https://gitlab.com/micro_project/docker.git ``
- copy file [.env.dev](.env.dev) to [.env](.env) and fill all variables for port mysql use 3306 better.
- run file init.sh that file will clone from repository backend and frontend try twice if some repo not cloned

### backend
- copy file [.env.example](.env.example) to [.env](.env) fill database data the ame like a docker mysql and uncomment MAILER_DSN and set smtp://localhost:1025 for send mails
- use this command for authentiaction `` php bin/console lexik:jwt:generate-keypair ``

### frontend
- Before started application you must build project `` npm run build `` or `` ng build ``

### next stage
- copy in docker file .env
- `` NGINX_BACKEND_DOMAIN=backendmicroapp.refs ``
- `` NGINX_FRONTEND_DOMAIN=frontendmicroapp.refs ``

### the last stage 
- start docker use this command `` docker compose up -d `` and go to browser
- example websites is hosts what you need in last variables http://backendmicroapp.refs:8081/ and in frontend http://frontendmicroapp.refs:8081/

## Mail sender
To check email you need use in backend this command `` php bin/console messenger:consume async `` this is neccessery for check mailhog email to test if you have some problems just change host in backend .env to 0.0.0.0 and paste this command the next change to docker name database host