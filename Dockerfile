FROM node:20.5.0-bullseye as build

WORKDIR /usr/share/nginx/html/frontend

COPY ./frontend /usr/share/nginx/html/frontend

ENV COMPOSER_ALLOW_SUPERUSER=1

RUN npm ci

RUN npm run ng build -- --configuration=development

FROM nginx:latest AS ngi

COPY --from=build /usr/share/nginx/html /usr/share/nginx/html/frontend

EXPOSE 80
